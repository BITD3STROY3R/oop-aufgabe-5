/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 5 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
  * @author Jannik Vierling 1226434
  */

public class Prefixed implements Dependent<Prefixed> {

	private String str = "";
	
	/* Vorbedingung: str != null
	 * Nachbedingung: Erzeugt ein neues Prefixed und initilisiert this.str
	 * mit str.
	 */
	public Prefixed(String str) {
		this.str = str;
	}

	/* Nachbedingung: Liefert den String der von diesem Objekt 
	 * gespeichert wird
	 * wird
	 */
	public String getString() {
		return str;
	}
	
	/* Vorbedingung: y != null
	 * Nachbedingung: Liefert true falls y.str ein Prefix von this.str 
	 * ist.*/
	@Override
	public boolean dependsOn(Prefixed y) {
		return str.indexOf(y.str) == 0;
	}
}
