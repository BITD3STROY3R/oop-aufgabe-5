/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 5 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
  * @author Jannik Vierling 1226434
  */
import java.util.Iterator;

/* Diese Klasse stellt eine Liste von Elementen dar die auf irgendeine
 * weise von anderen Objekten dieser Liste abhängen. Diese Klasse stellt
 * eine Methode zum einfügen, und zum Überprüfen der Konsistenz der Liste
 * zur Verfügung. Diese Klasse implementiert Iterable.
 */
public class DList<A extends Dependent<? super A>, B> extends AList<A,B> {

	/* Nachbedingung: Liefert true, wenn kein Element dieser Liste von 
	 * irgendeinem nachfolgenden Element dieser Liste abhängt.
	 */
	public boolean consistent() {
		SList<A> successors = copy(this);
		Iterator<A> itx = this.iterator();
		Iterator<A> ity = successors.iterator();
		boolean consistent = true;
		
		while (itx.hasNext() && consistent) {
			A x = itx.next();
			// das letzte Element hat keine Nachfolger
			if (itx.hasNext()) {
				ity.next();
				ity.remove();
				for (A s : successors) {
					consistent = consistent ? !x.dependsOn(s) : false;
				}
			}
		}
		return consistent;
	}
	
	/* Vorbedingung: list != null
	 * Nachbedingung: Liefert eine flache Kopie der Liste list. 
	 */
	private SList<A> copy(SList<A> list) {
		SList<A> copy = new SList<A>();
		
		for (A a : list) {
			copy.add(a, -1);
		}
		
		return copy;
	}
}
