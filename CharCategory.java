/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 5 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
  * @author Jannik Vierling 1226434
  */

public final class CharCategory extends Category {

	private char c;
	
	/* Nachbedingung: Erzeugt ein neues CharCategory und initialisiert es
	 * für c
	 */
	public CharCategory(char c) {
		this.c = c;
	}
	
	/* Vorbedingung: that != null
	 * Nachbedingung: Liefert true genau dann wenn that vom Typ CharCategory
	 * ist, und das von diesem Objekt gespeicherte Zeichen mit dem von
	 * that gleich ist.
	 */
	@Override
	protected boolean isDependingOn(Category that) {
		return that.isDependedBy(this);
	}

	/* Vorbedingung: left != null
	 * Nachbedingung: Liefert immer false
	 */
	@Override
	protected boolean isDependedBy(IntCategory left) {
		return false;
	}

	/* Vorbedingung: left != null
	 * Nachbedingung: Liefert true falls das in diesem Objekt gespeicherte
	 * Zeichen gleich dem von left ist.
	 */
	@Override
	protected boolean isDependedBy(CharCategory left) {
		return left.c == this.c;
	}

	/* Nachbedingung: Liefert die String Representation des Objekts in 
	 * der Form \{c\} */
	@Override
	public String toString() {
		return "{" + c + "}";
	}
}
