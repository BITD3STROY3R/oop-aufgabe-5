/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 5 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
  * @author Jannik Vierling 1226434
  */

import java.util.Iterator;
import java.util.NoSuchElementException;

/* Diese Klasse ermöglicht es Objekte und deren assoziierten Objekte zu
 * in einer bestimmten Reihenfolge zu speichern. Die Klasse stellt eine
 * Methode zum Einfügen zur verfügung, und implementiert Iterable. Der von
 * dieser Klasse gelieferte Iterator ermöglicht das entfernen von Objekten.
 */
public class AList<A,B> extends SList<A> implements Iterable<A> {
	
	private SList<SList<B>> associated = null;
	
	/* Nachbedingung: Erzeugt eine neue leere Liste */
	public AList() {
		associated = new SList<SList<B>>();
	}
	
	/* Iterator über der Liste AList */
	public final class AListIterator implements Iterator<A>, Iterable<B> {
		
		private Iterator<A> ita = AList.super.iterator();
		private Iterator<SList<B>> itb = associated.iterator();
		private SList<B> current = null;
		private boolean illegalIterator = true;
		private boolean illegalRemove = true;
		
		/* Nachbedingung: Erzeugt eine neues AListIterator Objekt, und
		 * initialisiert dieses für die aufrufenden Liste.
		 */
		private AListIterator() { }
		
		/* CCHC: next() muss vor jedem aufruf von remove() aufgerufen werde.
		 * Nachbedingung: Entfernt das zuletzt von next() zurückgegebene
		 * Element und die dazugehörige Liste assoziierter Elemente aus der
		 * Liste.
		 */
		public void remove() {
			if (illegalRemove) {
				throw new IllegalStateException();
			}
			illegalRemove = true;
			illegalIterator = true;
			
			ita.remove();
			itb.remove();
		}
		
		/* Nachbedingung: Liefert true falls die Liste Elemente enthält die
		 * noch nicht vom Iterator behandelt wurden.
		 */
		public boolean hasNext() {
			return ita.hasNext();
		}
		
		/* Vorbedingung: hasNext() == true
		 * Nachbedingung: Liefert das nächste Element aus der Liste.
		 */
		public A next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			
			illegalIterator = false;
			illegalRemove = false;
			current = itb.next();
			
			return ita.next();
		}
		
		/* CCHC: next() muss mindestens ein mal vor dieser Methode
		 * aufgerufen werden. Falls es einen aufruf von remove() gab,
		 * so muss vor dieser Methode next() aufgerufen werden.
		 * Nachbedingung: Liefert einen Iterator über die assoziierten 
		 * Elemente des aktuellen Elements. 
		 */
		public Iterator<B> iterator() {
			if (illegalIterator) {
				throw new IllegalStateException();
			}
			return current.iterator();
		}
	}
	
	/* Nachbedingung: Liefert einen Iterator über die Einträge dieser Liste,
	 * der es ermöglicht einen Iterator über die assoziierten Elemente eines
	 * Elements der Liste zu erzeugen.
	 */
	@Override
	public AListIterator iterator() {
		return new AListIterator();
	}
	
	/* Vorbedingung: -1 <= index < size
	 * Nachbedingung: Fügt das Element e nach dem index-ten Element ein und
	 * speichert eine leere Liste assozierter Elemente.
	 * Ist index = -1 so wird e am Ende der Liste hinzugefügt.
	 * Ist index = 0  so wird e am Anfang der Liste hinzugefügt.
	 */
	@Override
	public AList<A,B> add(A e, int index) {
		super.add(e, index);
		associated.add(new SList<B>(), index);
		
		return this;
	}
	
	/* note: Overload
	 * Vorbedingung: -1 <= index < size, list != null
	 * Nachbedingung: Fügt das Element e nach dem index-ten Element ein und
	 * speichert alle Elemente aus list in unveränderter Reihenfolge als
	 * Liste assozierter Elemente.
	 * Ist index = -1 so wird e am Ende der Liste hinzugefügt.
	 * Ist index = 0  so wird e am Anfang der Liste hinzugefügt.
	 */
	public AList<A,B> add(A e, int index, B list[]) {
		SList<B> assoc = new SList<B>();
		
		// build list
		for (B b : list) {
			assoc.add(b, -1);
		}
		
		super.add(e, index);
		associated.add(assoc, index);
		
		return this;
	}
	
	/* Nachbedingung: Liefert eine String Darstellung in folgender form
	 * [(Element:(Assoziierte Elemente), )*{Element:(Assoziierte Elemente)}]
	 */
	@Override
	public String toString() {
		String result = "[";
		Iterator<SList<B>> it = associated.iterator();
		boolean first = true;
		
		for (A a : this) {
			result += (first ? "" : ", ") + a + ":" + it.next();
			first = false;
		}
		
		return result + "]";
	}
}
