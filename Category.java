/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 5 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Jannik Vierling 1226434
 */

public abstract class Category implements Dependent<Category> {
	
	/* Invariante: Anzahl der Aufrufe von dependsOn() in die das Objekt
	 * war, weil entweder seine Methode dependsOn() aufgerufen wurde oder
	 * weil es als argument für einen solchen Aufruf verwendet wurde.
	 */
	private int uses = 0;
	
	/* SCHC: Erhöht für this und für that uses um eins.
	 * Nachbedingung: Liefert true falls that != null und die Klasse dieses
	 * Objekts gleich der Klasse von that ist, und diese Objekt auf
	 * irgendeine Weise von that abhängt.
	 */
	public boolean dependsOn(Category that) {
		this.uses++;
		that.uses++;
		
		return this.isDependingOn(that);
	}
	
	/* Vorbedingung: that != null
	 * Nachbedingung: Liefert true falls dieses Objekt auf irgendeine Weise
	 * von that abhängt.
	 */
	protected abstract boolean isDependingOn(Category that);
	
	/* Vorbedingung: left != null
	 * Nachbedingung: Liefert true falls left auf irgendeine Weise von
	 * diesem Objekt abhängt.
	 */
	protected abstract boolean isDependedBy(IntCategory left);

	/* Vorbedingung: left != null
	 * Nachbedingung: Liefert true falls left auf irgendeine Weise von
	 * diesem Objekt abhängt.
	 */
	protected abstract boolean isDependedBy(CharCategory left);
	
	
	/* Nachbedingung: Liefert die Anzahl der Aufrufe von dependsOn() in die
	 * dieses Objekt involviert war, weil entweder seine Methode dependsOn()
	 * aufgerufen wurde oder es als Arguement für einen solchen aufruf
	 * gedient hat.
	 */
	public int uses() {
		return uses;
	}
}
