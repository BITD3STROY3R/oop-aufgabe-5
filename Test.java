/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 5 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Iterator;

/*
 * @Author Bernd Bogomolov 1225256
 * @author Stefan Hanreich 1227486
 */
public class Test {

	private static final int pageWidth   = 45;
	private static final char headerChar = '=';
	private static final char sectionChar = '-';
	private static final int headerWidth = 45;
	private static final int labelMargin = 4;

	private static final CharCategory[] charCatArray1 = {new CharCategory('a'),
		new CharCategory('b'),
		new CharCategory('c'),
		new CharCategory('d')};

	private static final CharCategory[] charCatArray2 = {new CharCategory('e'),
		new CharCategory('f'),
		new CharCategory('g'),
		new CharCategory('h')};


	/* Vorbedingung: n >= 0
	 * Nachbedingung: Liefert einen String bestehend aus der N-Fachen
	 * wiederholung des Zeichens c.
	 */
	private static String repeat(int n, char c) {
		String result = "";

		for (int i = 0; i < n; i++) {
			result += c;
		}

		return result;
	}

	/* Vorbedingung: label != null
	 * Nachbedingung: Liefert einen String der maximal headerWidth Zeichen
	 * lang ist. Der String beginnt und endet mit maximal labelMargin vielen
	 * Leerzeichen. Die Anzahl der nachfolgenden Leerzeichen ist immer gleich
	 * der der Fuehrenden.
	 */
	private static String makeLabel(String label) {
		if (label.length() <= headerWidth) {
			int i = 0;
			while (i < labelMargin && label.length() <= headerWidth - 2) {
				label = " " + label + " ";
				i++;
			}
		} else {
			label = label.substring(0, headerWidth);
		}

		return label;
	}

	/* Vorbedingung: label != null
	 * Nachbedingung: Liefert einen String welcher eine formatierte
	 * Ueberschrift darstellt. Label ist der Text der Ueberschrift.
	 */
	private static String makeHeader(String label) {
		String header = "";

		header += repeat(headerWidth, headerChar) + '\n';
		label = makeLabel(label);
		header += repeat((headerWidth - label.length()) / 2, headerChar);
		header += label;
		header += repeat((headerWidth - label.length()) / 2
				+ (headerWidth - label.length()) % 2, headerChar);
		header += '\n';
		header += repeat(headerWidth, headerChar);

		return header;
	}

	/* Nachbedingung: Liefert einen String welcher eine Abtrennungszeile
	 * darstellt. Diese Zeile hat eine Breite von genau pageWidth zeichen.
	 */
	private static String makeSectionbar() {
		return repeat(pageWidth, sectionChar);
	}


	public static void main(String[] args) {
		test1();
		test2();
		DList<IntCategory, CharCategory> test3List = test3();
		test4(test3List);
		test5(test3List);
		test6(test3List);
	}
	
	/* 
	 * Nachbedingung: Eine DList gefuellt mit Testwerten
	 */
	public static DList<IntCategory, CharCategory> test3() {
		// PUNKT 3
		System.out.println(makeHeader("Punkt 3"));
		DList<IntCategory, CharCategory> dl
		= new DList<IntCategory, CharCategory>();

		dl.add(new IntCategory(1,2), -1, charCatArray1);
		dl.add(new IntCategory(2,3), -1, charCatArray2);
		dl.add(new IntCategory(3,4), -1, charCatArray1);
		dl.add(new IntCategory(4,5), -1, charCatArray2);


		AList<IntCategory, CharCategory>.AListIterator i3 = dl.iterator();	
		while(i3.hasNext()){
			String s = i3.next().toString() + ": { ";
			Iterator<CharCategory> i32 = i3.iterator();
			while(i32.hasNext()){
				s += i32.next().toString()+" ";
			}
			System.out.println(s + "}");
		}
		System.out.println("consistent: " + dl.consistent());

		System.out.println(makeSectionbar());

		dl.add(new IntCategory(4,5), -1, charCatArray1);
		dl.add(new IntCategory(5,6), -1, charCatArray1);

		i3 = dl.iterator();
		while(i3.hasNext()){
			String s = i3.next().toString() + ": { ";
			Iterator<CharCategory> i32 = i3.iterator();
			while(i32.hasNext()){
				s += i32.next().toString()+" ";
			}
			System.out.println(s + "}");
		}
		System.out.println("consistent: " + dl.consistent());

		return dl;
	}
	
	/* Vorbedingung: test3List != null
	 */
	public static void test4(AList<IntCategory, CharCategory> test3List) {

		System.out.println(makeHeader("Punkt 4"));

		test3List.add(new IntCategory(8,9), -1, charCatArray2)
		.add(new IntCategory(10,11), -1, charCatArray1)
		.add(new IntCategory(11,12), -1, charCatArray2);

		AList<IntCategory, CharCategory>.AListIterator i4 = test3List.iterator();	
		while(i4.hasNext()){
			String s = i4.next().toString() + ": { ";
			Iterator<CharCategory> i42 = i4.iterator();
			while(i42.hasNext()){
				s += i42.next().toString()+" ";
			}
			System.out.println(s + "}");
		}
	}
	
	/* Vorbedingung: test4List != null
	 */
	public static void test5(SList<IntCategory> test4List){

		System.out.println(makeHeader("Punkt 5"));

		test4List.add(new IntCategory(1,1), 0)
		.add(new IntCategory(13,14), -1)
		.add(new IntCategory(14,15), -1);

		Iterator<IntCategory> it = test4List.iterator();
		while(it.hasNext()){
			System.out.println(it.next());
		}
	}

	/* Vorbedingung: test345list != null
	 */
	public static void test6(AList<IntCategory, CharCategory> test345List) {

		System.out.println(makeHeader("Punkt 6"));

		DList<Category, Object> catList = new DList<Category, Object>();

		AList<IntCategory, CharCategory>.AListIterator it
		= test345List.iterator();

		while(it.hasNext()) {
			Category cat = it.next();

			Object[] assoc = {null, null};

			assoc[0] = cat.toString();
			assoc[1] = cat.uses(); 

			catList.add(cat, -1, assoc);

			for (Category c : it) {
				assoc[0] = c.toString();
				assoc[1] = cat.uses();

				catList.add(c, -1, assoc);	
			}
		}

		AList<Category, Object>.AListIterator it2 = catList.iterator();
		while(it2.hasNext()){
			String s = it2.next().toString() + ": { ";
			Iterator<Object> assocIt = it2.iterator();
			while(assocIt.hasNext()){
				s+=assocIt.next() + " ";
			}
			System.out.println(s + "}");
		}

		System.out.println("consistent: " + catList.consistent());

		System.out.println(makeSectionbar());
		System.out.println("Removing inconsistent Categories {4, 5}");
		it2 = catList.iterator();
		while(it2.hasNext()){
			Category c = it2.next();
			if(c.dependsOn(new IntCategory(4,5))
					|| c.dependsOn(new CharCategory('a'))
					|| c.dependsOn(new CharCategory('b'))
					|| c.dependsOn(new CharCategory('c'))
					|| c.dependsOn(new CharCategory('d'))
					|| c.dependsOn(new CharCategory('e'))
					|| c.dependsOn(new CharCategory('f'))
					|| c.dependsOn(new CharCategory('g'))
					|| c.dependsOn(new CharCategory('h'))
					){
				it2.remove();
			}			
		}

		System.out.println("consistent: " + catList.consistent());
	}

	/* Vorbedingung: keine
	 * Nachbedingung: keine
	 */
	public static void test2() {
		System.out.println(makeHeader("Punkt 2"));
		AList<String, Integer> al = new AList<String,Integer>();
		Integer intArray1[] = {1, 2, 3};
		Integer intArray2[] = {4, 5, 6};

		al.add("elem 1", -1, intArray1);
		al.add("elem 2", -1, intArray2);
		al.add("elem 3", -1, intArray1);
		al.add("elem 4", -1, intArray2);

		AList<String, Integer>.AListIterator i2 = al.iterator();		
		while(i2.hasNext()){
			String s = i2.next().toString() + ": { ";
			Iterator<Integer> i22 = i2.iterator();
			while(i22.hasNext()){
				s += i22.next().toString()+" ";
			}
			s += "}";
			System.out.println(s);
		}

		System.out.println(makeSectionbar());

		al.add("position 1", 1, intArray1);
		al.add("position 3", 3, intArray2);

		i2 = al.iterator();
		while(i2.hasNext()){
			String s = i2.next().toString() + ": { ";
			Iterator<Integer> i22 = i2.iterator();
			while(i22.hasNext()){
				s += i22.next().toString()+" ";
			}
			s += "}";
			System.out.println(s);
		}

	}

	/* Vorbedingung: keine
	 * Nachbedingung: keine
	 */
	public static void test1() {
		System.out.println(makeHeader("Punkt 1"));

		SList<Prefixed> sl = new SList<Prefixed>();

		sl.add(new Prefixed("Pre4"), 0);
		sl.add(new Prefixed("Pre3"), 0);
		sl.add(new Prefixed("Pre2"), 0);
		sl.add(new Prefixed("Pre1"), 0);

		Iterator<Prefixed> it = sl.iterator();

		while(it.hasNext()){
			System.out.println("Ad 1.1)  " + it.next().getString());
		}

		System.out.println(makeSectionbar());

		sl.add(new Prefixed("Pre2.2"), 2);
		sl.add(new Prefixed("Pre5"), 5);
		sl.add(new Prefixed("PreLast"), -1);

		it = sl.iterator();
		while(it.hasNext()){
			System.out.println("Ad 1.2)  " + it.next().getString());
		}

	}
}
