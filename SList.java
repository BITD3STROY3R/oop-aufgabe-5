/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 5 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
  * @author Jannik Vierling 1226434
  */

import java.util.Iterator;
import java.util.NoSuchElementException;

/* Diese Klasse ermoeglicht das speichern von Objekten in der Reihenfolge der
 * Indizes. Diese Klasse implementiert Iterable. Der von dieser Klasse 
 * gelieferte Iterator ermoeglicht das entfernen von Objekten.
 */
public class SList<A> implements Iterable<A> {

	private SListNode head = null;
	private SListNode end  = null;
	private int size = 0;
	
	private class SListNode {
		private SListNode next = null;
		private SListNode prev = null;
		private A data = null;
		
		/* Vorbedingung: prev != null
		 * Nachbedingung: Erzeugt einen neuen Listenknoten, welcher data
		 * data speichert und Nachfolger von prev ist.
		 */
		private SListNode(A data, SListNode prev) {
			this.data = data;
			this.next = prev.next;
			this.prev = prev;
			prev.next = this;
			if (this.next != null) {
				this.next.prev = this;
			}
		}
		
		/* Nachbedingung: Erzeugt einen neuen Listenknoten, welcher data
		 * speichert.
		 */
		private SListNode(A data) {
			this.data = data;
		}
	}

	private class SListIterator implements Iterator<A> {
		
		private SListNode current = head;
		private SListNode remove = null;
		private boolean illegalState = true;
		
		/* Nachbedingung: Liefert true falls es in der Liste noch Elemente
		 * gibt, die nicht vom Iterator behandelt wurden.
		 */
		@Override
		public boolean hasNext() {
			return current !=  null;
		}
		
		/* Vorbedingung: hasNext() == true
		 * Nachbedingung: Liefert das naechste Element der Liste
		 */
		@Override
		public A next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			illegalState = false;
			A data = current.data;
			remove = current;
			current = current.next;
			return data;
		}
		
		/* CCHC: next() muss vor jedem aufruf von remove() aufgerufen werden
		 * Nachbedingung: Entfernt das zuletzt von next() zurueckgegeben 
		 * Element aus der Liste.
		 */
		@Override
		public void remove() {
			if (illegalState) {
				throw new IllegalStateException();
			}
			illegalState = true;
			
			/* Nur ein Element in der Liste */
			if (remove == head && remove == end) {
				current = head = end = null;
			/* Am Anfang der Liste */
			} else if (remove == head) {
				head = head.next;
				head.prev = null;
			/* Am ende der Liste */
			} else if (remove == end) {
				end = end.prev;
				end.next = null;
			} else {
				remove.prev.next = remove.next;
				remove.next.prev = remove.prev;
			}
			size--;
		}
	}
	
	/* Vorbedingung: -1 <= index <= size
	 * Nachbedingung: Fuegt das Element e nach dem index-ten Element ein,
	 * ist index = -1 so wird e am Ende der Liste hinzugefuegt,
	 * ist index = 0  so wird e am Anfang der Liste hinzugefuegt.
	 */
	public SList<A> add(A e, int index) {
		
		if (-1 > index || index > size) {
			throw new ArrayIndexOutOfBoundsException(index);
		}
		// Am Ende einer nicht leeren Liste einfuegen
		if (index == -1 && size > 0) {
			end = new SListNode(e, end);
		// Ganz am Anfang einer nicht leeren Liste einfuegen 
		} else if (index == 0 && size > 0) {
			SListNode node = new SListNode(e);
			node.next = head;
			head.prev = node;
			head = node;
		// Nach dem index-ten Eintrag einfuegen
		} else if (0 < index && index <= size) {
			SListNode node = head;
			
			for (int i = 1; i < index; i++) {
				node = node.next;
			}
			SListNode newNode = new SListNode(e, node);
			if (index == size) {
				end = newNode;	
			}
		// In eine leere Liste einfuegen
		} else {
			head = end = new SListNode(e);
		}
		
		size++;
		
		return this;
	}
	
	/* Nachbedingung: Liefert einen Iterator ueber den Elementen dieser
	 * Liste. Der Iterator iteriert nach aufsteigenden Index ueber die 
	 * Elemente. */
	@Override
	public Iterator<A> iterator() {
		return new SListIterator();
	}
	
	/* Nachbedingung: Liefert eine String Darstellung der Liste */
	@Override
	public String toString() {
		String result = "[";
		SListNode node;
		boolean first = true;
		
		for (node = head; node != null; node = node.next) {
			result += (first ? "" : ", ") + node.data;
			first = false;
		}
		return result + "]";
	}
}
