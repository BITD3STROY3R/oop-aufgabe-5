/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 5 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
  * @author Jannik Vierling 1226434
  */

public final class IntCategory extends Category {

	private int x;
	private int y;
	
	/* Nachbedingung: Erzeugt ein neues IntCategory und intilisiert
	 * this.x mit x, und this.y mit y.
	 */
	public IntCategory(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/* Vorbedingung: that != null
	 * Nachbedingung: Liefert true falls dieses Objekt auf irgendeine eine
	 * Weise von that abhängt.
	 */
	@Override
	protected boolean isDependingOn(Category that) {
		return that.isDependedBy(this);
	}

	/* Vorbedingung: left != null
	 * Nachbedingung: Liefert true falls das x in left gleich dem von diesem
	 * Objekt ist, und falls das y in left größer gleich dem y in diesem
	 * Objekt ist.
	 */
	@Override
	protected boolean isDependedBy(IntCategory left) {
		return left.x == this.x && left.y >= this.y;
	}

	/* Vorbedingung: left != null || left == null 
	 * Nachbedingung: Liefert immer false 
	 */
	@Override
	protected boolean isDependedBy(CharCategory left) {
		return false;
	}

	/* Nachbedingung: Liefert die String Representation des Objekts in der
	 * Form \{x, y\}
	 */
	@Override
	public String toString() {
		return "{" + x + ", " + y + "}";
	}
}
